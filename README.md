# README #

This app will fetch the open data of pet adoption from Taipei city government. It use Core data to support off-line browsing. User can use this app to contact the shelter directly.

### TODO ###

* Icon and launch image design.

### Screen shot ###

![iOS Simulator Screen Shot 2015年1月18日 下午11.28.37.png](https://bitbucket.org/repo/oebAxd/images/256361784-iOS%20Simulator%20Screen%20Shot%202015%E5%B9%B41%E6%9C%8818%E6%97%A5%20%E4%B8%8B%E5%8D%8811.28.37.png)