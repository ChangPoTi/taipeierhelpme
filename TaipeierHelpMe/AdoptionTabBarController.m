//
//  AdoptionTabBarController.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/15.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "AdoptionTabBarController.h"
#import "AdoptionInfoViewController.h"

@interface AdoptionTabBarController ()

@end

@implementation AdoptionTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.viewControllers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        UINavigationController *navVC = (UINavigationController*)obj;
        AdoptionInfoViewController *adoptionViewController = (AdoptionInfoViewController*)navVC.visibleViewController;
        adoptionViewController.petType = idx;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
