//
//  AdoptionDataDownloader.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

@import CoreData;
#import "AdoptionDataDownloader.h"
#import "AdoptionCoreDataController.h"
#import "TCAPOAPIClient.h"
#import "PetAdoption.h"

NSString* const nAdoptionDataDownloaderHasError = @"kAdoptionDataDownloaderHasError";
NSString* const kDownloaderErrorKey = @"error";

@interface AdoptionDataDownloader()
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic) BOOL isDownloading;
@end

@implementation AdoptionDataDownloader

#pragma mark - Public methods

+ (instancetype)sharedDownloader {
    static AdoptionDataDownloader *_sharedDownloader = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDownloader = [[AdoptionDataDownloader alloc] init];
        _sharedDownloader.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];

        NSManagedObjectContext *parentContext = [AdoptionCoreDataController sharedController].managedObjectContext;
        [_sharedDownloader.managedObjectContext setParentContext:parentContext];
        
    });
    
    return _sharedDownloader;
}

- (void)startDownloadFromServer {
//- (void)startDownloadFromServerWithCompletion:(void (^)(BOOL))block {
    
    TCAPOAPIClient *client = [TCAPOAPIClient sharedClient];
    [client GET:@"datastore_search" parameters:@{@"resource_id" : @"c57f54e2-8ac3-4d30-bce0-637a8968796e",
                                                 @"limit" : @1}
        success:^(NSURLSessionTask *task, id response){
            self.isDownloading = NO;
            NSDictionary *result = [response valueForKey:@"result"];
            if (result) {
                [self downloadRecordsFromServer:[result valueForKey:@"total"]];
            }
            
        } failure:^(NSURLSessionTask *task, NSError *error){
            NSLog(@"downloader error: %@", error.userInfo);
            [self pushErrorNotification:error];
            self.isDownloading = NO;
        }];
    
    self.isDownloading = YES;
}

#pragma mark - Properties

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != managedObjectContext) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:_managedObjectContext];
        _managedObjectContext = managedObjectContext;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextDidSaveInPrivateQueue:) name:NSManagedObjectContextDidSaveNotification object:_managedObjectContext];
    }
}

#pragma mark - Private methods

#define ADOPTION_ENTITY_NAME @"PetAdoption"

- (void)downloadRecordsFromServer:(NSNumber*)amount {
    if (!amount) return;
    
    [[TCAPOAPIClient sharedClient] GET:@"datastore_search" parameters:@{@"resource_id" : @"c57f54e2-8ac3-4d30-bce0-637a8968796e",
                                                                        @"limit" : amount}
                               success:^(NSURLSessionTask *task, id response){
                                   self.isDownloading = NO;
                                   NSDictionary *result = [response valueForKey:@"result"];
                                   if (result) {
                                       [self.managedObjectContext performBlock:^{
                                           
                                           @autoreleasepool {
                                               [self deleteAllObjects:ADOPTION_ENTITY_NAME inManagedObjectContext:self.managedObjectContext];
                                               
                                               NSArray *records = [result valueForKey:@"records"];
                                               [records enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop){
                                                   [self insertPetAdoptionWithRecord:obj inManagedObjectContext:self.managedObjectContext];
                                               }];
                                           }
                                           
                                           NSLog(@"deleted count = %ld", self.managedObjectContext.deletedObjects.count);
                                           NSLog(@"inserted count = %ld", self.managedObjectContext.insertedObjects.count);
                                           
                                           NSError *error;
                                           if (![self.managedObjectContext save:&error]) {
                                               NSLog(@"Couldn't save records from server: %@", error.userInfo);
                                               [self pushErrorNotification:error];
                                           }
                                           
                                       }];
                                   }
                               }
                               failure:^(NSURLSessionTask *task, NSError *error){
                                   NSLog(@"download records error: %@", error.userInfo);
                                   [self pushErrorNotification:error];
                                   self.isDownloading = NO;
                               }];
    self.isDownloading = YES;
}

- (void)deleteAllObjects:(NSString *)entityDescription inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (!error) {
        [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
            [managedObjectContext deleteObject:obj];
        }];
    }
    
}

- (void)pushErrorNotification:(NSError*)error {
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:error, kDownloaderErrorKey, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:nAdoptionDataDownloaderHasError object:self userInfo:userInfo];
}

- (void)insertPetAdoptionWithRecord:(NSDictionary*)record inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext {
    PetAdoption *pet = [NSEntityDescription insertNewObjectForEntityForName:ADOPTION_ENTITY_NAME inManagedObjectContext:managedObjectContext];
    pet.name = [record valueForKey:@"Name"];
    pet.sex = [record valueForKey:@"Sex"];
    pet.type = [record valueForKey:@"Type"];
    pet.build = [record valueForKey:@"Build"];
    pet.age = [record valueForKey:@"Age"];
    pet.variety = [record valueForKey:@"Variety"];
    pet.acceptNum = [record valueForKey:@"AcceptNum"];
    pet.chipNum = [record valueForKey:@"ChipNum"];
    pet.isSterilization = [record valueForKey:@"IsSterilization"];
    pet.hairType = [record valueForKey:@"HairType"];
    pet.note = [record valueForKey:@"Note"];
    pet.resettlement = [record valueForKey:@"Resettlement"];
    pet.phone = [record valueForKey:@"Phone"];
    pet.email = [record valueForKey:@"Email"];
    pet.childAlong = [record valueForKey:@"ChildreAnlong"];
    pet.animalAlong = [record valueForKey:@"AnimalAnlong"];
    pet.bodyWeight = [record valueForKey:@"BodyWeight"];
    pet.imageName = [record valueForKey:@"ImageName"];
    pet.ckanId = [record valueForKey:@"_id"];
}

- (void)contextDidSaveInPrivateQueue:(NSNotification*)notification {
    NSManagedObjectContext *mainContext = [AdoptionCoreDataController sharedController].managedObjectContext;
    [mainContext performBlock:^{
        NSError *error;
        if (![mainContext save:&error]) {
            NSLog(@"save in main context, error: %@", error.userInfo);
        }
    }];
}
@end
