//
//  AdoptionDataDownloader.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const nAdoptionDataDownloaderHasError;
extern NSString* const kDownloaderErrorKey;

@interface AdoptionDataDownloader : NSObject
@property (nonatomic, readonly) BOOL isDownloading;

+ (instancetype)sharedDownloader;
- (void)startDownloadFromServer;
//- (void)startDownloadFromServerWithCompletion:(void (^)(BOOL success))block;
@end
