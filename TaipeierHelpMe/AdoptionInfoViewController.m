//
//  AdoptionInfoViewController.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/9.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//
@import CoreData;
#import "AdoptionInfoViewController.h"
#import "PetAdoption.h"
#import "AdoptionDataDownloader.h"
#import "AdoptionCoreDataController.h"
#import "PetTableViewCell.h"
#import "PetDetailViewController.h"

static void * const KVOContext = (void*)&KVOContext;

@interface AdoptionInfoViewController () <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@implementation AdoptionInfoViewController

#pragma mark - Properties
#define FETCH_BATCH_SIZE 20
- (NSFetchedResultsController *)fetchedResultController {
    if (!_fetchedResultController) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PetAdoption"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[self predicateStringOfPetType:self.petType]];
        [request setPredicate:predicate];
        
        [request setSortDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"acceptNum" ascending:YES]]];
        
        [request setFetchBatchSize:FETCH_BATCH_SIZE];
        
        _fetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[AdoptionCoreDataController sharedController].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        
        _fetchedResultController.delegate = self;
    }

    return _fetchedResultController;
}

#pragma mark - View controller methods

- (void)viewDidLoad {
    [super viewDidLoad];

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startDownloadFromServer) forControlEvents:UIControlEventValueChanged];
    [self.tableView insertSubview:self.refreshControl atIndex:0];
    
    UITableViewController *tableViewController = [[UITableViewController alloc] initWithStyle:self.tableView.style];
    tableViewController.tableView = self.tableView;
    tableViewController.refreshControl = self.refreshControl;
    [self addChildViewController:tableViewController];
    
    
    NSError *error;
    if (![self.fetchedResultController performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    //[[AdoptionDataDownloader sharedDownloader] startDownloadFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = [AdoptionDataDownloader sharedDownloader].isDownloading;
    [[AdoptionDataDownloader sharedDownloader] addObserver:self forKeyPath:@"isDownloading" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:KVOContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDownloaderError:) name:nAdoptionDataDownloaderHasError object:[AdoptionDataDownloader sharedDownloader]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[AdoptionDataDownloader sharedDownloader] removeObserver:self forKeyPath:@"isDownloading" context:KVOContext];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"push_pet_detail"]) {
        NSIndexPath *selectedIdx = [self.tableView indexPathForSelectedRow];
        PetAdoption *pet = [self.fetchedResultController objectAtIndexPath:selectedIdx];
        PetDetailViewController *vc = (PetDetailViewController*)segue.destinationViewController;
        vc.pet = pet;
    }
    
}

#pragma mark - Private Methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (context == KVOContext) {
        if ([keyPath isEqualToString:@"isDownloading"]) {
            BOOL active = [[change valueForKey:NSKeyValueChangeNewKey] boolValue];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = active;
            if (!active && self.refreshControl.isRefreshing)
                [self.refreshControl endRefreshing];
        }
    }
}

- (NSString*)predicateStringOfPetType:(PetType)type {
    switch (type) {
        case Dog:
            return @"type CONTAINS '犬'";
        case Cat:
            return @"type CONTAINS '貓'";
        default:
            return @"NOT ((type CONTAINS '犬') OR (type CONTAINS '貓'))";
    }
}

- (void)startDownloadFromServer {
    NSLog(@"startDonwloadFromServer");
    [[AdoptionDataDownloader sharedDownloader] startDownloadFromServer];
}

- (void)handleDownloaderError:(NSNotification*)notification {
    NSError *error = [notification.userInfo valueForKey:kDownloaderErrorKey];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ops", nil) message:error.localizedDescription delegate:nil cancelButtonTitle:NSLocalizedString(@"Confirm", nil) otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id  sectionInfo = [self.fetchedResultController.sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(PetTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    PetAdoption *info = [self.fetchedResultController objectAtIndexPath:indexPath];
    cell.title = [NSString stringWithFormat:@"%@, %@", info.name, info.type];
    cell.subtitle = info.note;
    cell.imageUrl = [NSURL URLWithString:info.imageName];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Set up the cell...
    [self configureCell:(PetTableViewCell*)cell atIndexPath:indexPath];
    
    return cell;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(PetTableViewCell*)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


/*- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}*/


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}
@end
