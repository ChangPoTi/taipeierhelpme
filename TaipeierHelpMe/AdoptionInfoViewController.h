//
//  AdoptionInfoViewController.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/9.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    Dog = 0,
    Cat,
    Other,
} PetType;

@interface AdoptionInfoViewController : UIViewController

@property (nonatomic) PetType petType;
@end
