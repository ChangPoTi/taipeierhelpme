//
//  TCAPOAPIClient.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "TCAPOAPIClient.h"

static NSString * const TCAPOAPIBaseURLString = @"http://210.65.114.15/api/action/";

@implementation TCAPOAPIClient

+ (instancetype)sharedClient {
    
    static TCAPOAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[TCAPOAPIClient alloc] initWithBaseURL:[NSURL URLWithString:TCAPOAPIBaseURLString]];
    });
    
    return _sharedClient;
}

@end
