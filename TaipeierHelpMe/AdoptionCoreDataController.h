//
//  AdoptionCoreDataController.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//
@import CoreData;
#import <Foundation/Foundation.h>

@interface AdoptionCoreDataController : NSObject
@property (strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, readonly) NSURL *applicationDocumentsDirectory;

+ (instancetype)sharedController;
@end
