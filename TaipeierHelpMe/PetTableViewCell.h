//
//  PetTableViewCell.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/15.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PetTableViewCell : UITableViewCell
@property (nonatomic, weak) NSString *title;
@property (nonatomic, weak) NSString *subtitle;
@property (nonatomic, copy) NSURL *imageUrl;
@end
