//
//  PetAdoption.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PetAdoption : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * build;
@property (nonatomic, retain) NSString * age;
@property (nonatomic, retain) NSString * variety;
@property (nonatomic, retain) NSString * reason;
@property (nonatomic, retain) NSString * acceptNum;
@property (nonatomic, retain) NSString * chipNum;
@property (nonatomic, retain) NSString * isSterilization;
@property (nonatomic, retain) NSString * hairType;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * resettlement;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * bodyWeight;
@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSString * childAlong;
@property (nonatomic, retain) NSString * animalAlong;
@property (nonatomic, retain) NSNumber * ckanId;

@end
