//
//  main.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/9.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
