//
//  PetTableViewCell.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/15.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "PetTableViewCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface PetTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *netImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@end

@implementation PetTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Properties

- (void)setImageUrl:(NSURL *)imageUrl {
    if (_imageUrl != imageUrl) {
        _imageUrl = imageUrl;
        [self.netImageView setImageWithURL:imageUrl];
    }
}

- (void)setTitle:(NSString *)title {
    [self.titleLabel setText:title];
}

- (NSString *)title {
    return self.titleLabel.text;
}

- (void)setSubtitle:(NSString *)subtitle {
    [self.subtitleLabel setText:subtitle];
}

- (NSString *)subtitle {
    return self.subtitleLabel.text;
}
@end
