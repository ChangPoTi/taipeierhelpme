//
//  PetDetailViewController.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/2/1.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PetAdoption.h"
@interface PetDetailViewController : UIViewController
@property (strong, nonatomic) PetAdoption *pet;
@end
