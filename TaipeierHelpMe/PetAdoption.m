//
//  PetAdoption.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "PetAdoption.h"


@implementation PetAdoption

@dynamic name;
@dynamic sex;
@dynamic type;
@dynamic build;
@dynamic age;
@dynamic variety;
@dynamic reason;
@dynamic acceptNum;
@dynamic chipNum;
@dynamic isSterilization;
@dynamic hairType;
@dynamic note;
@dynamic resettlement;
@dynamic phone;
@dynamic email;
@dynamic bodyWeight;
@dynamic imageName;
@dynamic childAlong;
@dynamic animalAlong;
@dynamic ckanId;

@end
