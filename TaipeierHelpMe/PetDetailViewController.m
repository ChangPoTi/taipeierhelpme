//
//  PetDetailViewController.m
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/2/1.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "PetDetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <TTTAttributedLabel.h>


@interface PetDetailViewController () <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *petImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *varietyLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *phoneLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *mailLabel;

@end

@implementation PetDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.pet) {
        [self.petImageView setImageWithURL:[NSURL URLWithString:self.pet.imageName]];
        self.nameLabel.text = self.pet.name;
        self.sexLabel.text = self.pet.sex;
        self.ageLabel.text = self.pet.age;
        self.varietyLabel.text = self.pet.variety;
        self.weightLabel.text = self.pet.bodyWeight;
        self.descriptionLabel.text = self.pet.note;
        self.phoneLabel.text = self.pet.phone;
        [self.phoneLabel addLinkToPhoneNumber:self.pet.phone withRange:NSMakeRange(0, self.pet.phone.length)];
        self.phoneLabel.delegate = self;
        
        self.mailLabel.text = self.pet.email;
        [self.mailLabel addLinkToURL:[NSURL URLWithString:self.pet.email] withRange:NSMakeRange(0, self.pet.email.length)];
        self.mailLabel.delegate = self;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]]];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", url]]];
}

@end
