//
//  AppDelegate.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/9.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

