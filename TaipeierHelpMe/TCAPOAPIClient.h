//
//  TCAPOAPIClient.h
//  TaipeierHelpMe
//
//  Created by Birdy Chang on 2015/1/10.
//  Copyright (c) 2015年 Birdy Chang. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface TCAPOAPIClient : AFHTTPSessionManager
+ (instancetype) sharedClient;
@end
